# HaloOnline-Pelican-0.6  
A half-assed port of the pelican mod from 0.5  

# Usage
To spawn it you must be in forge mode.
Unfortunatley the pelican can not be spawned normally via the forge menu.
Type "Forge.SpawnItem 0x05ADF" without quotes in the console to spawn it.

# Installation
Step 1: Go into maps folder in halo online directory and backup tags.dat and string_ids  
Step 2: Extract the mod into the maps folder.  
Step 3: Run the bat and delete mod folder and the cmds file including the bat.  

# Uninstall  
Step 1: Restore backed up tags and string_ids  
