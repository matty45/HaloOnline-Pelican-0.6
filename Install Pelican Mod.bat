@echo off
title Halo Online Tag Patcher
echo.
echo.
echo.
echo.
echo.
echo.
echo  Add-on    : Pelican Mod
echo.
echo  Version   : V0.5.0.2
echo.
echo  Type      : Tag import.
echo.
echo  Author    : Tillice and AltSierra117 , Ported to 0.6 by Matthew#0712
echo.
echo  Info      : This mod adds a fully functional combat vehicle.
echo.
echo.
echo.
echo.
echo  Press any key to start patching...
echo.
pause>nul
echo.
echo.
Type "pelican.cmds" | TagTool.exe
echo.
echo.
echo.
echo.
echo  Patching complete, Press any key to close...
echo.
pause>nul
